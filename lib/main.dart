import 'package:flutter/material.dart';
import 'package:aroma/views/views.dart';

const List<Widget> toggleViews = <Widget>[
  Text('To Roman Numeral'),
  Text('To Number'),
];

void main() {
  runApp(const AromaApplication());
}

class AromaApplication extends StatelessWidget {
  const AromaApplication({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aroma convertor',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.green,
      ),
      home: const AromaApplicationView(title: 'Aroma Roman Numeral convertor'),
    );
  }
}

class AromaApplicationView extends StatefulWidget {
  const AromaApplicationView({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<AromaApplicationView> createState() => _AromaApplicationState();
}

class _AromaApplicationState extends State<AromaApplicationView> {
  final List<bool> _selectedViewState = <bool>[true, false];
  bool vertical = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: <Widget>[
        ToggleButtons(
          direction: vertical ? Axis.vertical : Axis.horizontal,
          onPressed: (int index) {
            setState(() {
              // The button that is tapped is set to true, and the others to false.
              for (int i = 0; i < _selectedViewState.length; i++) {
                _selectedViewState[i] = i == index;
              }
            });
          },
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          selectedBorderColor: Colors.green[700],
          selectedColor: Colors.white,
          fillColor: Colors.green[200],
          color: Colors.white,
          constraints: const BoxConstraints(
            minHeight: 40.0,
            minWidth: 80.0,
          ),
          isSelected: _selectedViewState,
          children: toggleViews,
        ),
      ]),
      body: Center(
        child: Column(children: <Widget>[
          _selectedViewState[0]
              ? const ToRomanNumeralView(title: 'Convert to Roman Numeral')
              : const ToNumberView(title: 'Convert to number')
        ]),
      ),
    );
  }
}
