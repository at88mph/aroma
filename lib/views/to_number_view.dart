import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ToNumberView extends StatefulWidget {
  const ToNumberView({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<ToNumberView> createState() => _AromaState();
}

class _AromaState extends State<ToNumberView> {
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final romanNumeralFieldController = TextEditingController();

  String output = "";

  final RegExp symbolPattern = RegExp(
      r'(M*)?(CM)?(D)?(CD)?(C{0,3})(XC)?(L)?(XL)?(X{0,3})(IX)?(V)?(IV)?(I{0,3})');
  final Map<String, int> symbolValues = {
    "I": 1,
    "IV": 4,
    "V": 5,
    "IX": 9,
    "X": 10,
    "XL": 40,
    "L": 50,
    "XC": 90,
    "C": 100,
    "CD": 400,
    "D": 500,
    "CM": 900,
    "M": 1000
  };

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    romanNumeralFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Container(
      padding: const EdgeInsets.all(40.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const SizedBox(
            height: 18.0,
          ),
          TextField(
            controller: romanNumeralFieldController,
            decoration:
                const InputDecoration(labelText: "Enter a Roman Numeral"),
            keyboardType: TextInputType.text,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'[MDCLXVI]*'))
            ], // Only Roman Numeral values
          ),
          const SizedBox(height: 26.0),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () => calculate(),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Calculate", style: TextStyle(fontSize: 18.0)),
                ),
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          Text(
            output,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 48.0),
          )
        ],
      ),
    );
  }

  void calculate() {
    final String input = romanNumeralFieldController.text;
    final Iterable<MapEntry<String, int>> reversedSymbolValues =
        symbolValues.entries.toList().reversed;
    int result = 0;

    final RegExpMatch? match = symbolPattern.firstMatch(input);
    if (match == null) {
      throw ArgumentError("'$input' is invalid.");
    }

    // Skip the first one as it's the entire matched string.
    for (int i = 1; i <= match.groupCount; i++) {
      final MapEntry<String, int> entry = reversedSymbolValues.elementAt(i - 1);
      final String? nextMatch = match.group(i);
      if (nextMatch != null && nextMatch.trim() != "") {
        final int count = entry.key.allMatches(nextMatch).length;
        result += (count * entry.value);
      }
    }

    setState(() {
      output = "$result";
    });
  }
}
