import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ToRomanNumeralView extends StatefulWidget {
  const ToRomanNumeralView({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<ToRomanNumeralView> createState() => _AromaState();
}

class _AromaState extends State<ToRomanNumeralView> {
  // Create a text controller and use it to retrieve the current value
  // of the TextField.
  final numberFieldController = TextEditingController();

  String output = "";

  Map<String, int> symbolValues = {
    "I": 1,
    "IV": 4,
    "V": 5,
    "IX": 9,
    "X": 10,
    "XL": 40,
    "L": 50,
    "XC": 90,
    "C": 100,
    "CD": 400,
    "D": 500,
    "CM": 900,
    "M": 1000
  };

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    numberFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Container(
      padding: const EdgeInsets.all(40.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const SizedBox(
            height: 18.0,
          ),
          TextField(
            controller: numberFieldController,
            decoration: const InputDecoration(labelText: "Enter a number"),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(r'^[1-9]\d*'))
            ], // Only positive numbers allowed
          ),
          const SizedBox(height: 26.0),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () => calculate(),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Calculate", style: TextStyle(fontSize: 18.0)),
                ),
              ),
            ),
          ),
          const SizedBox(height: 16.0),
          Text(
            output,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 48.0),
          )
        ],
      ),
    );
  }

  void calculate() {
    final int input = int.parse(numberFieldController.text);
    final Iterable<MapEntry<String, int>> reversedSymbolValues =
        symbolValues.entries.toList().reversed;

    // Use a LinkedHashMap to preserve order.
    Map<String, int> outputMap = LinkedHashMap.fromEntries([]);
    _calculate(input, reversedSymbolValues, outputMap);

    String s = "";
    outputMap.entries.where((e) => e.value > 0).forEach((element) {
      for (int i = 0; i < element.value; i++) {
        s += element.key;
      }
    });

    setState(() {
      output = s;
    });
  }

  void _calculate(number, reversedSymbolValues, acc) {
    if (!reversedSymbolValues.isEmpty) {
      final MapEntry<String, int> entry = reversedSymbolValues.first;
      final int value = entry.value;
      final int count = _count(number, value);
      acc[entry.key] = count;
      _calculate(number - (count * value), reversedSymbolValues.skip(1), acc);
    }
  }

  int _count(i, value) {
    return (i / value).floor();
  }
}
